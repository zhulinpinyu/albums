import React, { Component } from 'react';
import { View, Text, Image, Linking } from 'react-native';

import Card from './Card';
import CardSection from './CardSection';
import Button from './Button';

export default class AlbumDetail extends Component {
  render() {
    const { album } = this.props;
    return (
      <Card>
        <CardSection>
          <View style={styles.thumbViewStyle}>
            <Image
              style={styles.thumbStyle}
              source={{ uri: album.thumbnail_image }}
            />
          </View>
          <View style={styles.headerContentStyle}>
            <Text style={styles.headerTextStyle}>{album.title}</Text>
            <Text>{album.artist}</Text>
          </View>
        </CardSection>
        <CardSection>
          <Image
            style={styles.imageStyle}
            source={{ uri: album.image }}
          />
        </CardSection>
        <CardSection>
          <Button onPress={() => Linking.openURL(album.url)}>
            Buy Now
          </Button>
        </CardSection>
      </Card>
    );
  }
}

const styles = {
  thumbStyle: {
    width: 50,
    height: 50
  },
  thumbViewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 6,
    marginRight: 10
  },
  headerContentStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  headerTextStyle: {
    fontSize: 18
  },
  imageStyle: {
    height: 300,
    flex: 1,
    width: null
  }
};
