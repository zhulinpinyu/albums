Albums: 专辑信息展示的Simple App
===

Thanks: [https://www.udemy.com/the-complete-react-native-and-redux-course/learn/v4/content](https://www.udemy.com/the-complete-react-native-and-redux-course/learn/v4/content)

### 运行效果：

![Albums](./albums.gif)

### 开发环境运行：

```
yarn
react-native run-ios
```